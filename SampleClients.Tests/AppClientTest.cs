using Xunit;

namespace SampleClients.Tests
{
    public sealed class AppClientTest
    {
        [Fact]
        public void Test()
        {
            var actual = AppClient.Hello();
            
            Assert.Equal("[こんにちは、世界!]", actual);
        }
    }
}