using Samples;

namespace SampleClients
{
    public static class AppClient
    {
        public static string Hello()
        {
            return $"[{HelloWorld.Text()}]";
        }
    }
}